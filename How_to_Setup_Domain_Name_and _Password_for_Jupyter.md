## How to Setup Domain Name & Password for Jupyter

##### If we only did what is outlined in How to Set Up an AWS Instance, Docker, and Jupyter, recall that we need the IPv4 address of our instance and our Jupyter notebook's token to access our notebook. IP addresses and tokens are not memorable. Most people have to look up their addresses and tokens. However, we do not need our IP address and token if we associate our IP address with a domain name and setup a password. 

![browser showing domain name in address bar and request for password](src/jupyter_domain_pw.png)

### How to Setup Domain Name
#### 1. Buy a Domain Name
I bought a domain from the web hosting company 1and1 only for its promotional offer. In February 2018, you could get a domain for a year for only $1. Just go to 1and1.com and how to buy a domain name should be self-explanatory. 
#### 2. Set Your Domain's IP Address to Your AWS Instance's IPv4 Address
With 1and1, I just logged in 1and1.com and editted my domain's DNS settings. 

### How to Setup a Password for Your Jupyter Notebook
According to [Jupyter's Documentation](http://jupyter-notebook.readthedocs.io/en/stable/public_server.html), new versions of Jupyter give you the opportunity to setup a password from the user interface. The following are directions if that option is not available.

#### 1. Open Git BASH
#### 2. SSH into your AWS instance by running `ssh ubuntu@[instance_ipv4_addr]`
#### 3. Get the name the Docker container running Jupyter by running `docker ps` 
For information about `docker ps`, go to this [link](https://docs.docker.com/engine/reference/commandline/ps/)
If you do not have a container that is running Jupyter, create a container running Jupyter. Look to How to Set Up an AWS Instance, Docker, and Jupyter for directions.
#### 4. Execute an interactive bash shell on the Jupyter container by running  `docker exec -it [container_name]  bash`
For information about `docker exec`, go to this [link](https://docs.docker.com/engine/reference/commandline/exec/#usage)
#### 5. Run `jupyter notebook password`
The shell will tell you to simply enter and verify your password. Run the same command if you want to reset your password. You do not need your old password to reset it.
